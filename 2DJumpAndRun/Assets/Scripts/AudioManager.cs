﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CoATwoPR
{
    public class AudioManager : MonoBehaviour
    {
        //wenn eine andere Szene geladen wird, wird dieses Gameobjekt nicht zerstört.
        void Awake()
        {
            DontDestroyOnLoad(this);
        }
    }
}