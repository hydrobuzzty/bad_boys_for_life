﻿using System;
using System.Collections;
using System.Collections.Generic;
using CoATwoPR;
using UnityEngine;

namespace CoATwoPR
{
    public class Bullet : MonoBehaviour
    {
        public float speed = 20f;

        public int damage = 50;

        public GameObject barrier;

        public GameObject bullet;

        public Rigidbody2D rb;
        
        //setzt die bullet-velocity
        private void Start()
        {
            rb.velocity = transform.right * speed;
        }

        private void OnTriggerEnter2D(Collider2D hitInfo)
        {
            //wenn bullet mit einem GameObject collided, dann soll die TakeDamage Methode aufgerufen werden und das GameObject zerstört werden
            if (hitInfo.gameObject.CompareTag("Enemy"))
            {
                Enemy enemy = hitInfo.GetComponent<Enemy>();
                if (enemy != null)
                {
                    enemy.TakeDamage(damage);
                }
                Destroy(gameObject);
            }
            //bullet ignoriert barrier-collider, sonst fliegt sie nicht vom Spieler weg, sondern nur gegen das BarrierItem
            else
            {
                Physics2D.IgnoreCollision(barrier.GetComponent<Collider2D>(), bullet.GetComponent<Collider2D>());
            }
        }
    }
}