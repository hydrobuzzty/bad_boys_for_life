﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class Coin : MonoBehaviour
    {
        [SerializeField]
        private int score = 20;

        public void OnTriggerEnter2D(Collider2D other)
        {
           //wenn der Trigger vom Coin von player getreten wird, wird das gameobjekt deaktiviert
           //und durch den enemyManager bekommt man die Punkte gutgeschrieben
            if (other.gameObject.CompareTag("Player"))
            {
                EnemyManager manager = GameObject.FindWithTag("EnemyManager").GetComponent<EnemyManager>();
                gameObject.SetActive(false);
                manager.Process(gameObject, true, score);
            }
        }
    }
} 