using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class DestroyAudioManager : MonoBehaviour
    {
        // Zerstört das GameObjekt mit dem Namen AudioManager.
        void Start()
        {
            Destroy(GameObject.Find("AudioManager"));
        }
    }
}