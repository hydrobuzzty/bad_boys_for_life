﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class Enemy : MonoBehaviour
    {
        public int health = 100;
    
        [SerializeField]
        private int score;

        //damage soll von health abgezogen werden. Wenn health <= 0, dann soll die Die Methode aufgerufen werden
        public void TakeDamage(int damage)
        {
            health -= damage;

            if (health <= 0)
            {
                Die();
            }
        }

        //wenn das Objekt einen bestimmten Tag hat, dann soll es deaktiviert weredn und der Score soll erhöht werden
        private void Die()
        {
            EnemyManager manager = GameObject.FindWithTag("EnemyManager").GetComponent<EnemyManager>();
            gameObject.SetActive(false);
            manager.Process(gameObject, true, score);
        }
    }
}    
