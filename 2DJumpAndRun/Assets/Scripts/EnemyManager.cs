using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class EnemyManager : MonoBehaviour
    {
        [SerializeField]
        private HighScore highScore;
        
        public void Process(GameObject Enemy, bool raiseHighScore, int score)
        {
            // We check if its allowed to raise the HighScore
            if (raiseHighScore)
            {
                // if it's allowed we call the AddHighScore() Method and pass in our score value
                highScore.AddHighScore(score);
            }
        }
    }
}