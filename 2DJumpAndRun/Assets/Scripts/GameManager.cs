﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CoATwoPR
{
    public class GameManager : MonoBehaviour
    {
        
        [SerializeField]
        private HighScore highScore;
        [SerializeField]
        private GameObject startingText;
        [SerializeField]
        private GameObject jumpingText;
        [SerializeField]
        private GameObject flyingText;
        [SerializeField]
        private GameObject shootingText;
        [SerializeField]
        private GameObject barrierText;
        [SerializeField]
        private GameObject barrelText;
        [SerializeField]
        private GameObject portalText;
        [SerializeField] 
        private GameObject redBarrel;
    
        //wenn der Spieler mit einem Collider kollidiert, dann sollen GameObjects aktiviert oder deaktiviert werden
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("jumpingText"))
            {
                startingText.SetActive(false);
                jumpingText.SetActive(true);
            }
            
            if (other.gameObject.CompareTag("flyingText"))
            {
                jumpingText.SetActive(false);
                flyingText.SetActive(true);
            }
            
            if (other.gameObject.CompareTag("shootingText"))
            {
                flyingText.SetActive(false);
                shootingText.SetActive(true);
            }
            
            if (other.gameObject.CompareTag("barrierText"))
            {
                shootingText.SetActive(false);
                barrierText.SetActive(true);
            }
            
            if (other.gameObject.CompareTag("barrelText"))
            {
                barrierText.SetActive(false);
                barrelText.SetActive(true);
            }
            //wenn der Spieler mit einem GameObject mit bestimmten Tag kollidiert,
            //dann soll eine Szene gealden werden
            if (other.gameObject.CompareTag("Portal"))
            {
                highScore.SaveHighScore();
                SceneManager.LoadScene("LevelHub");
            }
        }
    
        //wenn GameObject deaktiviert wird, soll ein anderes aktiviert werden
        private void Update()
        {
            if (!redBarrel.activeSelf)
            {
                barrelText.SetActive(false);
                portalText.SetActive(true);
            }
        }
    }

}
