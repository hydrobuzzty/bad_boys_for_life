using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CoATwoPR
{
    public class HighScore : MonoBehaviour
    {
        /// <summary>
        /// Serialized reference to the TextMeshProUGUI component, assign this in the Inspector
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI textMesh;
        
        /// <summary>
        /// Internally stored int variable for the HighScore
        /// </summary>
        private int currentHighScore;
        
        private void Update()
        {
            // The textMesh displays the currentHighScore every frame
            textMesh.text = currentHighScore.ToString();
        }

        /// <summary>
        /// Adds to the current HighScore
        /// </summary>
        /// <param name="score"></param>
        public void AddHighScore(int score)
        {
            // With a simple addition we raise the highscore
            currentHighScore += score;
            
        }

        public void SaveHighScore()
        {
            // The string property .persistentDataPath in the Application class of Unity points
            // to a very specific folder, a directory path where you can store data that you want to be kept between runs
            // It's a different location for each platform, please lookup the Scripting Manual
            // Since we create a file or overwrite an existing file when saving we have to define the filePath
            // We combine the persistentDataPath, effectively the folder where the savefile will be, with a custom file, consisting
            // of a name and the filetype - since we will use the JsonUtility for saving and loading, it's wise to set .json as the
            // filetype
            
            
            string filePath = Application.persistentDataPath + "/HighScoreLv1.json";
            // We create an instance of the HighScoreData class since this is what we want to save, the complete instance
            HighScoreData highScoreData = new HighScoreData();

            // We overwrite the highScore value of that instance with our currentHighScore value
            highScoreData.highScore = currentHighScore;

            // We translate the highScoreData with the .ToJson method into the json text format
            // and assign it a string which will hold this formatted json text
            string dataToSave = JsonUtility.ToJson(highScoreData);

            // With the System.IO class File we can utilize the .WriteAllText method in order to actually
            // create a new text file at the specified filePath and populate that file with specific contents, in
            // our case the highScoreData which was formatted as a json text and stored in the local string variable
            // If a file with the same filename exists at that path it will get overwritten
            File.WriteAllText(filePath, dataToSave);
        }
    }
}