﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CoATwoPR
{
    public class Intro : MonoBehaviour
    {
        public GameObject Bild2;
        public GameObject Bild3;
        public GameObject Bild4;
       
        
        //Startet die Coroutine
        void Start()
        {
            StartCoroutine(IntroCoroutine());
        }
        
        //wartet die Zahl in den Klammern als Sekunden ab
        //bevor immer ein weiteres bild aktivert wird.
        IEnumerator IntroCoroutine()
        {
            yield return new WaitForSeconds(8);
            Bild2.SetActive(true);
            yield return new WaitForSeconds(7);
            Bild3.SetActive(true);
            yield return new WaitForSeconds(7);
            Bild4.SetActive(true);
            yield return new WaitForSeconds(3);
            SceneManager.LoadScene("LevelHub");



        }
    }
}