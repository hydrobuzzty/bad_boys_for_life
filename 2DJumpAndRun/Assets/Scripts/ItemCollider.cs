﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace CoATwoPR
{
    public class ItemCollider : MonoBehaviour
    {
        public GameObject jetItem;

        public GameObject shootItem;

        public GameObject barrierItem;

        public GameObject barrier;

        private string jetItemTag;

        private string shootItemTag;

        private string barrierItemTag;

        private bool deactivateItem = false;
    

        //wenn der Spieler mit einem gameObject mit einem bestimmten Tag kollidiert, dann soll ein anderes GameObject deaktiviert/aktiviert werden
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("JetItem"))
            {
                jetItem.SetActive(false);
            }

            if (collision.gameObject.CompareTag("ShootingItem"))
            {
                shootItem.SetActive(false);
            }

            if (collision.gameObject.CompareTag("BarrierItem"))
            {
                barrierItem.SetActive(false);
                barrier.SetActive(true);
            }
        }
    }
 
}
