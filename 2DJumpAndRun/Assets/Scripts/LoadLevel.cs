﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CoATwoPR
{
    public class LoadLevel : MonoBehaviour
    {
        //läd bestimmte Szene, wenn Methode aufgerufen wird
        public void LoadingLvL01()
        {
            SceneManager.LoadScene("LVL01");
        }
    
        public void LoadingLvL02()
        {
            SceneManager.LoadScene("Lvl02");
        }
    
        public void LoadingLvL03()
        {
            SceneManager.LoadScene("Lvl03");
        }
    }
}