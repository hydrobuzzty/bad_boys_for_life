﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace CoATwoPR
{
    public class Player : MonoBehaviour
    {
        private float health = 100f;
        public CharacterController2D controller;

        [SerializeField] private HighScore highScore;

        public Animator animator;

        private float horizontalMove = 0f;

        private float runSpeed = 40f;

        [SerializeField] private bool jump = false;

        private bool crouch = false;
        [SerializeField] private float jetForce;
        [SerializeField] private float jetWait;
        [SerializeField] private float jetRecovery;
        [SerializeField] private float max_fuel;
        [SerializeField] private float current_fuel;
        [SerializeField] private float current_recovery;
        [SerializeField] private bool canJet;

        [SerializeField] private bool jetDelay = false;

        private Transform ui_fuelbar;

        private Rigidbody2D _rigidbody2D;
        public GameObject jetItem;

        public GameObject shootingItem;

        public GameObject barrierItem;

        public GameObject barrier;
    
        public AudioSource deathClipSource;

        public AudioClip deathClip;

        public GameObject deathImage;

        public string levelToLoad;

        public GameObject fuelBar;

        public GameObject texts;

        public GameObject player;

        public GameObject environment;

        private bool enemyDamage = false;
        

        private bool loadScene = false;

        [SerializeField] private LayerMask mWhatIsGround;

        [SerializeField] private Transform mGroundCheck;

        const float KGroundedRadius = .2f;

        [SerializeField]
        private bool _mGrounded;

        public UnityEvent onLandEvent;
    

        //setzt den delayBool auf true
        private void JetDelay()
        {
            jetDelay = true;
        }
        
        private void Start()
        {
            _rigidbody2D = GetComponent<Rigidbody2D>();
            current_fuel = max_fuel;

            ui_fuelbar = GameObject.FindWithTag("FuelBar").transform;

            deathClipSource.clip = deathClip;
        }
    
        //zerstört Gameobject wenn aufgerufen
        public void DestroyBarrier()
        {
            Destroy(barrier);
            enemyDamage = true;
        }

        //wenn der Spieler Landet, sollen Bool-Werte geändert werden
        public void OnLanding()
        {    
            animator.SetBool("IsJumping", false);
            animator.SetBool("IsFlying", false);
            jump = false;
            jetDelay = false;
        }

        public void OnTriggerEnter2D(Collider2D other)
        {
            //wenn der Spieler mit einem GameObject mit bestiten Tag kollidiert, soll health auf 0 gesetzt werden
            if (other.gameObject.CompareTag("acid"))
            {
                health = 0f;
            }

            if (other.gameObject.CompareTag("coin"))
            {
                
            }

            if (other.gameObject.CompareTag("Enemy") && enemyDamage)
            {
                health = 0;
            }
        }

        //läd Szene
        void GameOverLoadScene()
        {
            SceneManager.LoadScene("LevelHub");
        }
        
        // deaktiviert verschiedene GameObjects wenn Methode aufgerufen wird
        private void GameOver()
        {
            deathClipSource.PlayOneShot(deathClip);        
            environment.SetActive(false);
            player.SetActive(false);
            fuelBar.SetActive(false);
            texts.SetActive(false);
            deathImage.SetActive(true);
            highScore.SaveHighScore();
            //läd Szene nach 4 Sekunden
            Invoke(nameof(GameOverLoadScene), 4f);
        }

        private void Update()
        {
            if (health <= 0f)
            {
                GameOver();
            }

            //setzt Bewegung des Spielers mit UnityInputSystem * runSpeed
            horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

            animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

            //wenn GameObject deaktiviert ist soll anim-bool geändert werden
            if (!shootingItem.activeSelf)
            {
                animator.SetBool("Shooting", true);
            }

            //wenn "Jump" gedrückt wird sollen bool-werte geändert werden und ein Delay gestartet werden
            if (Input.GetButtonDown("Jump"))
            {
                Debug.Log("jumping");
                jump = true; 
                animator.SetBool("IsJumping", true); 
                current_recovery = 0f; 
                Invoke("JetDelay", 0.4f);
            }

            //wenn jump = true, setzt recovery auf 0
            if (jump)
            {
                current_recovery = 0f;
            }

            //wenn GameObject deaktiviert ist, soll anderes GameObject aktiviert werden
            if (!barrierItem.activeSelf)
            {
                barrier.SetActive(true);
            }

            //wenn GameObject aktiviert ist setzt health auf 100
            if (barrier.activeSelf)
            {
                Invoke(nameof(DestroyBarrier), 10);
            }
        }

        private void FixedUpdate()
        { 
            //greift auf Move Methode des controllers zu
            controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
            
            //wenn "Jump" gedrückt wird, dann wird jet auf true gesetzt
            bool jet = Input.GetButton("Jump");

            bool wasGrounded = _mGrounded;

            _mGrounded = false;
            //der Spieler ist grounded wenn der circlecast mit dem ground kollidiert
            Collider2D[] colliders = Physics2D.OverlapCircleAll(mGroundCheck.position, KGroundedRadius, mWhatIsGround);
        
            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].gameObject != gameObject)
                {
                    _mGrounded = true;
                    if (wasGrounded)
                        onLandEvent.Invoke();
                }
            }
            
            
            if (jetDelay && !_mGrounded && !jetItem.activeSelf)
            {
                canJet = true;
            }


            if (_mGrounded)
            {
                canJet = false;
            }

            //wenn der Spieler fliegt und jet erlaubt ist, dann wird force auf den Spieler ausgewirkt und seine Animation verändert
            if (canJet && jet && current_fuel > 0)
            {
                animator.SetBool("IsFlying", true);
                _rigidbody2D.AddForce(Vector3.up * (jetForce * Time.fixedDeltaTime), (ForceMode2D) ForceMode.Acceleration);
                current_fuel = Mathf.Max(0, current_fuel - Time.fixedDeltaTime);
            }
            else
            {
                animator.SetBool("IsFlying", false);
            }

            if (_mGrounded)
            {
                if (current_recovery < jetWait)
                {
                    current_recovery = Mathf.Min(jetWait, current_recovery + Time.fixedDeltaTime);
                }
                else
                {
                    current_fuel = Mathf.Min(max_fuel, current_fuel + Time.fixedDeltaTime * jetRecovery);
                }
            }

            //Verändert die fuelBar, wenn der Spieler fliegt
            ui_fuelbar.localScale = new Vector3(current_fuel / max_fuel, 1, 1);
        }
    }
}
