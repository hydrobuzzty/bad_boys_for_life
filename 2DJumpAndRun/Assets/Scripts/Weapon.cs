﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class Weapon : MonoBehaviour
    {
        public Transform firePoint;

        public GameObject bulletPrefab;

        public GameObject shootingItem;

        //wenn "K" gedrückt wird, soll "Shoot" aufgerufen werden
        private void Update()
        {
            if (!shootingItem.activeSelf)
            {
                if (Input.GetButtonDown("Fire1"))
                {
                    Shoot();
                }
            }
        }

        //feuert bulletPrefab ab
        void Shoot()
        {
            Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        }
    }

}
